from django.shortcuts import render

# Create your views here.
def kemampuan(request):
    response = {
        'judul': 'Kemampuan'
    }
    return render(request, 'kemampuan.html', response)