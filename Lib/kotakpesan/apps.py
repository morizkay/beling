from django.apps import AppConfig


class KotakpesanConfig(AppConfig):
    name = 'kotakpesan'
