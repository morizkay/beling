from django.shortcuts import render
from bukutamu.forms import Message_Form
from bukutamu.models import Message
from django.http import HttpResponseRedirect

# Create your views here.
def beranda(request):
    response = {}
    response = {
        'judul' : 'Beranda'
    }
    response['message_form'] = Message_Form
    return render(request, 'beranda.html', response)

def message_post(request):
    response = {}
    form = Message_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name'] if request.POST['name'] != "" else "Anonymous"
        response['email'] = request.POST['email'] if request.POST['email'] != "" else "Anonymous"
        response['message'] = request.POST['message']
        message = Message(
            name=response['name'], 
            email=response['email'], 
            message = response['message'])
        message.save()
        html ='form_result.html'
        return render(request, html, response)
    else:
        return HttpResponseRedirect('/')

def message_table(request):
    message = Message.objects.all()
    response = {
        'judul': 'Buku Tamu'
    }
    response['message'] = message
    html = 'table.html'
    return render(request, html , response)