from django import forms

class Message_Form(forms.Form):
    error_messages = {
        'required' : 'Harap isi masukan dengan lengkap',
        'invalid' : 'Harap isi alamat email dengan benar'
    }
    attrs = {
        'class' : 'form-control'
    }

    name = forms.CharField(label='Nama', required=False, max_length=27, empty_value='Anonymous', widget=forms.TextInput(attrs=attrs))
    email = forms.EmailField(required=True, widget=forms.EmailInput(attrs=attrs))
    message = forms.CharField(widget=forms.Textarea(attrs=attrs), required=True)