from django.shortcuts import render

# Create your views here.
def portofolio(request):
    response = {
        'judul': 'Portofolio'
    }
    return render(request, 'portofolio.html', response)