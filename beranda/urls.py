from django.urls import path, re_path
from .views import beranda, message_post, message_table

app_name = 'beranda'

urlpatterns = [
    path('', beranda, name='beranda'),
    path('message_post', message_post, name='message_post'),
    path('result_table', message_table, name='result_table')
]